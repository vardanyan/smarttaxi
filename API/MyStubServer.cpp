/* 
 * File:   MyStubServer.cpp
 * Author: David
 * 
 * Created on 2 ноября 2015 г., 12:11
 */

#include "MyStubServer.h"
#include <stdio.h>
#include <stdarg.h>
#include <my_global.h>
#include <mysql.h>

MyStubServer::MyStubServer(AbstractServerConnector &connector) :
    AbstractStubServer(connector)
{
}
void MyStubServer::notifyServer()
{
    cout << "Server got notified" << endl;
}
void finish_with_error(MYSQL *con)
{
  fprintf(stderr, "%s\n", mysql_error(con));
  mysql_close(con);
  exit(1);        
}

Json::Value MyStubServer::login(const string &name)
{
  MYSQL *con = mysql_init(NULL);
  
  mysql_options(con, MYSQL_SET_CHARSET_NAME, "utf8"); 
  mysql_options(con, MYSQL_INIT_COMMAND, "SET NAMES utf8"); 
  
  if (con == NULL) 
  {
      fprintf(stderr, "%s\n", mysql_error(con));
      exit(1);
  }

  if (mysql_real_connect(con, "localhost", "root", "Divad369", 
          "SmartTaxi", 0, NULL, 0) == NULL) 
  {
      fprintf(stderr, "%s\n", mysql_error(con));
      mysql_close(con);
      exit(1);
  }  

  
  char c_sql[255];
  sprintf(c_sql,  "SELECT * FROM users WHERE user_name=N'%s'",name.c_str());
  
  if (mysql_query(con, c_sql)) 
  {  
      fprintf(stderr, "%s\n", mysql_error(con));
      mysql_close(con);
      exit(1);
  }

  MYSQL_RES *result = mysql_store_result(con);
  
  if (result == NULL) 
  {
      finish_with_error(con);
  }

  int num_fields = mysql_num_fields(result);

  MYSQL_ROW row;
  Json::Value array;
  int n_row=0;
  while ((row = mysql_fetch_row(result))) 
  { 
      Json::Value val;
      for(int i = 0; i < num_fields; i++) 
      { 
	  if(i==0)val["id"]=row[i];
          if(i==1)val["user_name"]=row[i];
	  if(i==2)val["user_email"]=row[i];
	  if(i==3)val["user_phone"]=row[i];
          
          printf("%s ", row[i] ? row[i] : "NULL"); 
      } 
      array[n_row]=val;
      printf("\n");
      n_row+=1; 
  }
  mysql_close(con);
  return array;
}


