/* 
 * File:   MyStubServer.h
 * Author: David
 *
 * Created on 2 ноября 2015 г., 12:11
 */

#ifndef MYSTUBSERVER_H
#define	MYSTUBSERVER_H

#include "abstractstubserver.h"
#include <stdio.h>
#include <stdarg.h>
#include <jsonrpccpp/server/connectors/httpserver.h>

using namespace jsonrpc;
using namespace std;

class MyStubServer : public AbstractStubServer {
public:
        MyStubServer(AbstractServerConnector &connector);

        virtual void notifyServer();
        virtual Json::Value login(const std::string& name);

};

#endif	/* MYSTUBSERVER_H */

